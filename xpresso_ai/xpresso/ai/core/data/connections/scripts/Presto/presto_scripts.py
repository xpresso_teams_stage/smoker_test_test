"""

    Class design for running scripts to install presto server on a standalone machine
    NOTE: Editing required for a few properties before installation

"""

__author__ = 'Shlok Chaudhari'
__all__ = 'dataconnector'

import subprocess
import os
import editor
import json
from xpresso.ai.core.data.connections.scripts.Presto.presto_config import presto_catalogs
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser


class PrestoServerAutomation:
    """

    Automating Presto server setup for a standalone machine

    """

    def __init__(self):
        """

        __init__() function here initializes the xpresso logger module and
        the XprConfigParser

        XprConfigParser specifies IP and Port for Presto Server setup
        """

        self.logger = XprLogger()
        with open(XprConfigParser.DEFAULT_CONFIG_PATH) as config_file:
            presto_config = json.load(config_file).get('presto')
        self.presto_host_ip = presto_config.get('presto_ip')
        self.presto_host_port = presto_config.get('presto_port')

    def server_setup(self):
        """

        Scripts to setup presto server which functions both as a Co-ordinator
        and a Worker running on same machines.

        :return: None
        """

        os.chdir("/opt")
        subprocess.run(args=['wget', 'https://repo1.maven.org/maven2/com/'
                                     'facebook/presto/presto-server'
                                     '/0.224/presto-server-0.224.tar.gz'],
                       stdout=subprocess.PIPE, text=True)
        subprocess.run(args=['tar', '-zxf',
                             'presto-server-0.224.tar.gz'], stdout=subprocess.PIPE, text=True)
        os.chdir("/opt/presto-server-0.224")
        subprocess.run(args=['mkdir', 'data'], stdout=subprocess.PIPE, text=True)
        self.server_properties_setup()
        self.server_cli_setup()
        self.logger.info("Server Setup Complete")

    def server_properties_setup(self):
        """

        Script to setup Node and JVM properties

        :return: None
        """

        subprocess.run(args=['mkdir', 'etc'], stdout=subprocess.PIPE, text=True)
        os.chdir("/opt/presto-server-0.224/etc")
        editor.edit(filename="node.properties", contents=b"node.environment = production\n"
                                                         b"node.id = ffffffff-ffff-"
                                                         b"ffff-ffff-ffffffffffff\n"
                                                         b"node.data-dir = "
                                                         b"/presto-server-0.224/data")
        editor.edit(filename="jvm.config", contents=b"-server\n-Xmx16G\n-XX:+UseG1GC"
                                                    b"\n-XX:G1HeapRegionSize=32M"
                                                    b"\n-XX:+UseGCOverheadLimit"
                                                    b"\n-XX:+ExplicitGCInvokesConcurrent"
                                                    b"\n-XX:+HeapDumpOnOutOfMemoryError"
                                                    b"\n-XX:+ExitOnOutOfMemoryError")
        editor.edit(filename="config.properties", contents=b"coordinator=true\n"
                                                           b"node-scheduler."
                                                           b"include-coordinator=true\n"
                                                           b"http-server.http.port="
                                                           + bytes(self.presto_host_port
                                                                   + "\n", 'ascii') +
                                                           b"query.max-memory=5GB\n"
                                                           b"query.max-memory-per-node=1GB\n"
                                                           b"query.max-total-"
                                                           b"memory-per-node=2GB\n"
                                                           b"discovery-server.enabled=true\n"
                                                           b"discovery.uri="
                                                           b"http://"
                                                           + bytes(self.presto_host_ip
                                                                   + ":"
                                                                   + self.presto_host_port
                                                                   , 'ascii'))
        editor.edit(filename="log.properties", contents=b"com.facebook.presto = INFO")
        subprocess.run(args=['mkdir', 'catalog'], stdout=subprocess.PIPE, text=True)
        os.chdir("/opt/presto-server-0.224/etc/catalog")
        editor.edit(filename="jmx.properties", contents=b"connector.name = jmx")
        for key in presto_catalogs:
            editor.edit(filename=key, contents=bytes(presto_catalogs[key], 'ascii'))
        os.chdir("/opt/presto-server-0.224")
        self.logger.info("Properties and Catalogs created successfully")

    def server_cli_setup(self):
        """

        Script to setup Presto Command-Line Interface

        :return: None
        """

        subprocess.run(args=['wget', 'https://repo1.maven.org/maven2/com/'
                                     'facebook/presto/presto-cli/0.224'
                                     '/presto-cli-0.224-executable.jar'],
                       stdout=subprocess.PIPE, text=True)
        subprocess.run(['mv', 'presto-cli-0.224-executable.jar',
                        'presto'], stdout=subprocess.PIPE, text=True)
        subprocess.run(args=['chmod', '+x', 'presto'], stdout=subprocess.PIPE, text=True)
        self.logger.info("CLI setup complete")

    def start_server(self):
        """

        Script to start presto service as a daemon process

        :return: None
        """

        subprocess.run(args=['bin/launcher', 'start'], stdout=subprocess.PIPE, text=True)
        self.logger.info("Presto server is running")


if __name__ == '__main__':
    auto_server = PrestoServerAutomation()
    auto_server.server_setup()
    auto_server.start_server()
